<?php

namespace LoanTest\Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tests\TestCase;

class LoanControllerTest extends TestCase
{
    public function testGetAllLoans()
    {
        $response = $this->get('/loans');

        $response->seeStatusCode(200)
                 ->seeJsonStructure(['data']);
    }

    public function testCreateLoan()
    {
        $loanData = [
            'amount' => 1000,
            'duration' => 12,
            'interest_rate' => 5.5,
        ];

        $response = $this->post('/loans', $loanData);

        $response->seeStatusCode(201)
                 ->seeJsonStructure(['data']);
    }

    public function testGetLoanById()
    {
        $loanId = 1;

        $response = $this->get("/loans/{$loanId}");

        $response->seeStatusCode(200)
                 ->seeJsonStructure(['data']);
    }

    public function testGetNonExistentLoan()
    {
        $loanId = 9999;

        $response = $this->get("/loans/{$loanId}");

        $response->seeStatusCode(404)
                 ->seeJsonStructure(['data']);
    }

    public function testUpdateLoan()
    {
        $loanId = 1;
        $updatedLoanData = [
            'amount' => 1200,
            'duration' => 10,
            'interest_rate' => 6.0,
        ];

        $response = $this->put("/loans/{$loanId}", $updatedLoanData);

        $response->seeStatusCode(200)
             ->seeJsonStructure(['data']);
    }

    public function testUpdateNonExistentLoan()
    {
        $loanId = 9999;
        $updatedLoanData = [
            'amount' => 1200,
            'duration' => 10,
            'interest_rate' => 6.0,
        ];

        $response = $this->put("/loans/{$loanId}", $updatedLoanData);

        $response->seeStatusCode(404)
             ->seeJson(['error' => 'Loan not found']);
    }

    public function testDeleteLoan()
    {
        $loanId = 1;

        $response = $this->delete("/loans/{$loanId}");

        $response->seeStatusCode(200)
             ->seeJson(['message' => 'Loan deleted successfully']);
    }

    public function testDeleteNonExistentLoan()
    {
        $loanId = 9999;

        $response = $this->delete("/loans/{$loanId}");

        $response->seeStatusCode(404)
        ->seeJson(['error' => 'Loan not found']);
    }
}
