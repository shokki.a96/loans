<?php

namespace App\Jobs;

use App\Models\Loan;

class LoanJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function create(array $data): Loan
    {
        return Loan::create($data);
    }


    public function getLoanById(int $id): ?Loan
    {
        return Loan::find($id);
    }


    public function updateLoan(int $id, array $data): ?Loan
    {
        $loan = Loan::find($id);

        if (!$loan) {
            return null; // Loan not found
        }

        $loan->update($data);

        return $loan;
    }


    public function deleteLoan(int $id): bool
    {
        $loan = Loan::find($id);

        if (!$loan) {
            return false; // Loan not found
        }

        return $loan->delete();
    }


    public function getLoans(array $filters = []): array
    {
        $query = Loan::query();

        if (isset($filters['created_at'])) {
            $query->whereDate('created_at', $filters['created_at']);
        }

        if (isset($filters['amount'])) {
            $query->where('amount', $filters['amount']);
        }

        return $query->get()->toArray();
    }
}
