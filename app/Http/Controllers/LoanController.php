<?php

namespace App\Http\Controllers;

use App\Jobs\LoanJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UpdateLoanRequest;

class LoanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(protected LoanJob $loanJob,)
    {
    }

    public function index(Request $request)
    {
        $filters = $request->only(['created_at', 'amount']);

        $loans = $this->loanJob->getLoans($filters);

        return response()->json(['data' => $loans]);
    }


    public function createLoan(Request $request)
    {
        // Validate the request
        $rules = [
            'amount' => 'required|numeric',
            'duration' => 'required|numeric',
            'interest_rate' => 'required|numeric',
        ];

        // Perform validation
        $validator = Validator::make($request->all(), $rules);

        // Check if validation fails
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // Delegate loan creation to the service
        $loan = $this->loanJob->create($request->all());

        return response()->json(['data' => $loan], 201);
    }



    public function show($id)
    {
        $loan = $this->loanJob->getLoanById($id);

        if (!$loan) {
            return response()->json(['data' => 'Loan not found'], 404);
        }

        return response()->json(['data' => $loan], 200);
    }



    public function update(Request $request, $id)
    {
        // Validate the request
        $rules = [
            'amount' => 'required|numeric',
            'duration' => 'required|numeric',
            'interest_rate' => 'required|numeric',
        ];

        // Perform validation
        $validator = Validator::make($request->all(), $rules);

        // Check if validation fails
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }


        $loan = $this->loanJob->updateLoan($id, $request->all());

        if (!$loan) {
            return response()->json(['error' => 'Loan not found'], 404);
        }

        return response()->json(['data' => $loan]);
    }


    public function destroy($id)
    {
        $loan = $this->loanJob->deleteLoan($id);

        if (!$loan) {
            return response()->json(['error' => 'Loan not found'], 404);
        }

        return response()->json(['message' => 'Loan deleted successfully']);
    }
}
