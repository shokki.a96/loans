Инструкции по использованию API
Описание
API предоставляет возможность управления займами.

Установка
Клонируйте репозиторий:
bash
Копировать код
git clone <repository-url>
Перейдите в каталог проекта:
bash
Копировать код
cd <project-directory>
Установите зависимости:
bash
Копировать код
composer install
Настройте файл окружения .env:
bash
Копировать код
cp .env.example .env
Измените параметры базы данных в файле .env:

makefile
Копировать код
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=your_database
DB_USERNAME=your_username
DB_PASSWORD=your_password
Выполните миграции для создания таблиц в базе данных:
bash
Копировать код
php artisan migrate
Запустите веб-сервер:
bash
Копировать код
php -S localhost:8000 -t public
Использование
Создание нового займа
POST /loans

Endpoint для создания нового займа.

Пример запроса:

bash
Копировать код
curl -X POST http://localhost:8000/loans -d '{"amount": 1000, "duration": 30}'
Получение информации о займе
GET /loans/{id}

Endpoint для получения информации о займе по его ID.

Пример запроса:

bash
Копировать код
curl http://localhost:8000/loans/1
Обновление информации о займе
PUT /loans/{id}

Endpoint для обновления информации о займе.

Пример запроса:

bash
Копировать код
curl -X PUT http://localhost:8000/loans/1 -d '{"amount": 1500}'
Удаление займа
DELETE /loans/{id}

Endpoint для удаления займа по его ID.

Пример запроса:

bash
Копировать код
curl -X DELETE http://localhost:8000/loans/1
Получение списка всех займов
GET /loans

Endpoint для получения списка всех займов с базовыми фильтрами по дате создания и сумме.

Пример запроса:

bash
Копировать код
curl http://localhost:8000/loans
Заметки
Все данные передаются в формате JSON.
При отправке запросов необходимо предоставить корректные данные.
Перед использованием API убедитесь, что сервер запущен и база данных настроена правильно.